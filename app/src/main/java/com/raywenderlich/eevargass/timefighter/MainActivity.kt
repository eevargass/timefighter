package com.raywenderlich.eevargass.timefighter

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {

    internal lateinit var tapMeButton : Button
    internal lateinit var yourScoreText : TextView
    internal lateinit var timeLeftText : TextView

    internal lateinit var countDownTimer: CountDownTimer
    internal val initialCountDown : Long = 10000
    val ONE_SECOND : Long = 1000
    val DEFAULT_TIME : Long = initialCountDown/ONE_SECOND

    private var timeLeftInTimer = initialCountDown
    private var started:Boolean = false
    private var myTime : Long = DEFAULT_TIME
    private var myScore :Int = 0

    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val SCORE_KEY = "SCORE_KEY"
        private const val TIME_KEY = "TIME KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG, "your score is: $myScore" )
        this.tapMeButton = findViewById(R.id.buttonTapMe)
        yourScoreText = findViewById(R.id.textScore)
        timeLeftText = findViewById(R.id.textTime)

        if(savedInstanceState!=null){
            myScore = savedInstanceState.getInt(SCORE_KEY)
            timeLeftInTimer = savedInstanceState.getLong(TIME_KEY)
            restoreGame()
        }else{
            resetGame()
        }

        tapMeButton.setOnClickListener { elBoton ->
            val bounceAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce_animation)
            elBoton.startAnimation(bounceAnimation)
            val textBlink = AnimationUtils.loadAnimation(this,R.anim.text_blink)
            yourScoreText.startAnimation(textBlink)
            this.tapMeClick()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.my_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.actionAbout){
            showInfo()
        }
        return true
    }

    @SuppressLint("StringFormatInvalid")
    private fun showInfo(){
        val dialogTitle = getString(R.string.app_name, BuildConfig.VERSION_NAME)
        val dialogMessage = getString(R.string.about_message)

        val builder = AlertDialog.Builder(this)
        builder.setTitle(dialogTitle)
        builder.setMessage(dialogMessage)
        builder.create().show()

        Toast.makeText(this, getString(R.string.about_message) , Toast.LENGTH_LONG).show()
    }

    private fun restoreGame() {
        yourScoreText.text = getString(R.string.your_score, myScore)
        myTime = timeLeftInTimer/ONE_SECOND
        timeLeftText.text = getString(R.string.time_left, myTime)
        countDownTimer = object : CountDownTimer(timeLeftInTimer, ONE_SECOND){
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisecondsUntilFinished: Long) {
                timeLeftInTimer = millisecondsUntilFinished
                myTime = millisecondsUntilFinished/ONE_SECOND
                timeLeftText.text = getString(R.string.time_left, myTime)
            }
        }
        started = true
        countDownTimer.start()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(TIME_KEY, timeLeftInTimer)
        outState.putInt(SCORE_KEY, myScore)
        countDownTimer.cancel()
        Log.d(TAG, "values were saved to outState object. Time is: $timeLeftInTimer and Score is $myScore")
    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        /* myScore = savedInstanceState.getInt(SCORE_KEY)
        timeLeftInTimer = savedInstanceState.getLong(TIME_KEY)  */
        Log.d(TAG, "values were recovered from outState object. Time is: $timeLeftInTimer and Score is $myScore")
    }

    private fun resetGame(){
        myTime = DEFAULT_TIME
        myScore = 0
        started = false
        yourScoreText.text = getString(R.string.your_score, myScore)
        timeLeftText.text = getString(R.string.time_left, myTime)
        countDownTimer = object : CountDownTimer(initialCountDown, ONE_SECOND){
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisecondsUntilFinished: Long) {
                timeLeftInTimer = millisecondsUntilFinished
                myTime = millisecondsUntilFinished/ONE_SECOND
                timeLeftText.text = getString(R.string.time_left, myTime)
            }
        }
    }

    private fun tapMeClick(){
        if (!started){
            startGame()
        }
        myScore++
        //timeLeftText.text = getString(R.string.time_left) + myTime.toString()
        // es preferible no concatenar sino inyectar el valor dentro de la cadena, tal como se hace abajo
        yourScoreText.text = getString(R.string.your_score, myScore)
    }

    private fun startGame() {
        resetGame()
        started = true
        countDownTimer.start()
    }

    private fun endGame(){
        Toast.makeText(this, getString(R.string.TIME_IS_UP, myScore ), Toast.LENGTH_LONG).show()
        resetGame()
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(TAG,"se reinicio")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"se detuvo")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG,"se destruyo" )
    }
}